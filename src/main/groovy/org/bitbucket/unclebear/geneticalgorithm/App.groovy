package org.bitbucket.unclebear.geneticalgorithm

class App {
    static void main(String[] args) {
        def population = Long.valueOf(System.getProperty("population", "1000"))
        def selection = Double.valueOf(System.getProperty("selection", "0.9"))
        def timeout = Long.valueOf(System.getProperty("timeout", "60"))
        def generation = Long.valueOf(System.getProperty("generation", "10000"))
        def top = Double.valueOf(System.getProperty("top", "0.01"))
        def output = System.getProperty("output", "output.txt")
        GeneticAlgorithm.start(population, selection, timeout, generation, top, output)
    }
}
