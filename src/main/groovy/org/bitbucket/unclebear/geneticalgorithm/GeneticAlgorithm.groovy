package org.bitbucket.unclebear.geneticalgorithm

import groovy.util.logging.Slf4j

@Slf4j
class GeneticAlgorithm {
    static def start(populationSize, selectionRatio, timeout, generation, topRatio, outputFile) {
        def milestones = []
        0.1.step(1, 0.1) { milestones.add((generation * it) as int) }
        log.info("Now generating the initial population, please wait")
        def population = generatePopulation(populationSize)
        log.info("Now starting evolution, please wait")
        def startTime = System.currentTimeMillis() / 1000
        def endTime = startTime + timeout
        def i = 0
        while (true) {
            def now = System.currentTimeMillis() / 1000
            if (now >= endTime) {
                log.info("Run out of $timeout seconds after $i generations")
                break
            }
            if (i >= generation) {
                log.info("Completed $generation generations in ${now - startTime} seconds")
                break
            }
            if (isTopStabilized(population, topRatio)) {
                log.info("Top ${topRatio * 100}% population stabilized")
                break
            }
            ++i
            def selectedPopulation = select(population, selectionRatio)
            def newPopulation = generatePopulation(populationSize - selectedPopulation.size())
            population = selectedPopulation + newPopulation
            if (milestones.stream().parallel().anyMatch { it == i }) log.info("Completed $i of $generation generations, continuing")
        }
        log.info("Top ${topRatio * 100}% population will be kept")
        population = select(population, topRatio)
        log.info("Completed evolution")
        saveToFile(population, outputFile)
    }

    static def generatePopulation(size) { (1..size).stream().parallel().collect { IndividualFactory.get() } }

    static def isTopStabilized(population, ratio) {
        def top = select(population, ratio)
        top.stream().parallel().allMatch { it.fitness == top[0].fitness }
    }

    static def select(population, ratio) {
        def sorted = new TreeSet<>()
        sorted.addAll(population)
        sorted.headSet(sorted[(sorted.size() * ratio) as int])
    }

    static def saveToFile(population, file) {
        new File(file).withWriter { writer -> population.each { writer.println(it) } }
    }
}
