package org.bitbucket.unclebear.geneticalgorithm

class Individual implements Comparable {
    def x1, x2, x3, x4, y, fitness

    Individual() {
        while (true) {
            x1 = RandomFactory.get()
            x2 = RandomFactory.get() * (1 - x1)
            x3 = RandomFactory.get() * (1 - x1 - x2)
            x4 = 1 - x1 - x2 - x3
            if (isValid()) break
        }
        y = 32906277.22 * x1**2
          + 34514969.19 * x2**2
          + 29190000.46 * x3**2
          + 34613158.58 * x4**2
          + 67388324.55 * x1 * x2
          + 61979726.73 * x1 * x3
          + 67493891.61 * x1 * x4
          + 63476432.28 * x2 * x3
          + 69111148.28 * x2 * x4
          + 63562019.85 * x3 * x4
          - 33726588.36 * x1
          - 34541851.79 * x2
          - 31758602.89 * x3
          - 34602300.44 * x4
          + 34643645.95
        fitness = y * -1
    }

    def isValid() { [x1, x2, x3, x4].stream().parallel().allMatch { it >= 0 } && x1 + x2 + x3 + x4 == 1 }

    @Override
    String toString() { sprintf("x1=%f, x2=%f, x3=%f, x4=%f, y=%f", x1, x2, x3, x4, y) }

    @Override
    int compareTo(Object o) { o.fitness - fitness }
}
