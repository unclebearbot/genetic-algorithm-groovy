package org.bitbucket.unclebear.geneticalgorithm

class IndividualFactory {
    static def preFetch = new PreFetch<Individual>({ new Individual() })

    static def get() { preFetch.get() }
}
