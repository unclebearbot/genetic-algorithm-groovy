package org.bitbucket.unclebear.geneticalgorithm

class RandomFactory {
    static def random = new Random()
    static def preFetch = new PreFetch<Double>({ random.nextDouble() })

    static def get() { preFetch.get() }
}
