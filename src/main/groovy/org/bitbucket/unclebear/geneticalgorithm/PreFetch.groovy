package org.bitbucket.unclebear.geneticalgorithm

import java.util.concurrent.LinkedBlockingQueue

class PreFetch<T> {
    static final def BUFFER_SIZE = Math.sqrt(Runtime.getRuntime().totalMemory()) as int
    static final def THREAD_COUNT = Runtime.getRuntime().availableProcessors() * 2 - 1
    def buffer = new LinkedBlockingQueue<T>(BUFFER_SIZE)

    PreFetch(Closure<T> closure) {
        def thread = new Thread({ while (true) (1..THREAD_COUNT).stream().parallel().forEach { buffer.put(closure.call()) } })
        thread.daemon = true
        thread.start()
    }

    def get() { buffer.take() }
}
